------------------------------------------------------------------------------------------
-- AutoProfitLite - r3
--
-- Automatically sell junk items when you click the button on the vendors window.
--
-- Send suggestions, comments, and bugs to bug@kanadian.net.
------------------------------------------------------------------------------------------

local AddOnName = "AutoProfitLite";
local DBName = "AutoProfitLiteDB";
local Version = GetAddOnMetadata( AddOnName, "Version" );

-- Create our addon.
AutoProfitLite = LibStub( "AceAddon-3.0" ):NewAddon( AddOnName,
                                              "AceConsole-3.0",
                                              "AceEvent-3.0" );

local L = LibStub( "AceLocale-3.0" ):GetLocale( AddOnName, false );

local LDB = LibStub( "LibDataBroker-1.1", true );
local LDBIcon = LDB and LibStub( "LibDBIcon-1.0", true );

local totalProfit = 0;

APLUser = ( "%s@%s" ):format( GetUnitName( "player" ), GetRealmName() );

local aplDefaults = {
     global = {
          apxb = { hide = false, },
          Temp = {},
          showGreeting = true,
          Version = false,
     },
     char = {
          debug = {
               state = false,
               temp = {};
          },
          autoSell = false,
          autoSellReport = false,
          showTotal = true,
          buttonSpin = 3,
          spinRate = 0.6,
          profits = 1,
          cText = 1,
     },
}

local aplAuthor = {
     ["Lindarena@Laughing Skull"] = true,
     ["St�bs�lot@Laughing Skull"] = true,
     ["Hilyna@Laughing Skull"] = true,
     ["Nagdand@Laughing Skull"] = true,
     ["Holycurves@Laughing Skull"] = true,
     ["Holytree@Laughing Skull"] = true,
     ["Acatra@Laughing Skull"] = true,
     ["N�sh@Laughing Skull"] = true,
     ["Loansone@Laughing Skull"] = true,
     ["Koton@Laughing Skull"] = true,
}

local Config = {
     Spinner = {
          { name = L["APL_MISC_NEVER"], value = 0, },
          { name = L["APL_MISC_MOUSEOVER_PROFIT"], value = 1, },
          { name = L["APL_MISC_MOUSEOVER"], value = 2, },
          { name = L["APL_MISC_PROFIT"], value = 3, },
     },
     colorType = {
          { name = L["APL_COLORLESS"], value = 1, },
          { name = L["APL_COLORED"], value = 2, },
     },
     printType = {
          { name = L["APL_TEXT"], value = 1, },
          { name = L["APL_COIN"], value = 2, },
     },
}

local apl = {
     type = "group",
     name = L["APL_ADDON_LABEL"],
     args = {
          confgendesc = {
               type = "description", order = 1,
               name = L["APL_ABOUT_DESC"],
               cmdHidden = true,
          },
          cat1 = {
               type = "group", order = 2, inline = true,
               name = L["APL_ABOUT"],
               args = {
                    confversiondesc = {
                         type = "description", order = 2,
                         name = L["APL_ABOUT_VERSION"]:format( GetAddOnMetadata( AddOnName, "Version" ) ),
                         cmdHidden = true,
                    },
                    confauthordesc = {
                         type = "description", order = 3,
                         name = L["APL_ABOUT_AUTHOR"]:format( GetAddOnMetadata( AddOnName, "Author" ) ),
                         cmdHidden = true,
                    },
                    confcatdesc = {
                         type = "description", order = 4,
                         name = L["APL_ABOUT_CATEGORY"]:format( GetAddOnMetadata( AddOnName, "X-Category" ) ),
                         cmdHidden = true,
                    },
                    confemaildesc = {
                         type = "description", order = 5,
                         name = L["APL_ABOUT_EMAIL"]:format( GetAddOnMetadata( AddOnName, "X-Email" ) ),
                         cmdHidden = true,
                    },
                    confwebsitedesc = {
                         type = "description", order = 6,
                         name = L["APL_ABOUT_WEBSITE"]:format( GetAddOnMetadata( AddOnName, "X-Website" ) ),
                         cmdHidden = true,
                    },
                    confmemorydesc = {
                         type = "description", order = 7,
                         name = function()
                              UpdateAddOnMemoryUsage();
                              mem = floor( GetAddOnMemoryUsage( AddOnName ) );
                              return L["APL_ABOUT_MEMORY"]:format( mem );
                         end,
                         cmdHidden = true,
                    },
               },
          },
          cat2 = {
               type = "group", order = 3,
               name = L["APL_ADDON_LABEL"],
               args = {
                    loadmsg = {
                         type = "toggle", order = 1,
                         name = L["APL_ABOUT_LOAD"],
                         desc = L["APL_ABOUT_LOAD_DESC"],
                         get = function() return AutoProfitLite.db.global.showGreeting; end,
                         set = function( _, value )
                              PlaySound( value and "igMainMenuOptionCheckBoxOff" or "igMainMenuOptionCheckBoxOff" );
                              AutoProfitLite.db.global.showGreeting = value; 
                         end,
                    },
                    optdebug = {
                         type = "toggle", order = 2,
                         name = L["APL_DEBUG_OPTDEBUG"],
                         desc = L["APL_DEBUG_OPTDEBUG_DESC"],
                         hidden = function() return not aplAuthor[ APLUser ]; end,
                         get = function() return AutoProfitLite.db.char.debug.state; end,
                         set = function( _, value )
                              AutoProfitLite.db.char.debug.state = value; 
                         end,
                    },
                    desc = {
                         type = "description", order = 3, width = "full",
                         name = "\n",
                         cmdHidden = true,
                    },
               },
          },
          cat3 = {
               type = "group", order = 4,
               name = L["APL_SETTINGS_LABEL"],
               args = {
                    autoSell = {
                         type = "toggle", order = 1,
                         name = L["APL_AUTOSELL"],
                         desc = L["APL_AUTOSELL_DESC"],
                         get = function () return AutoProfitLite.db.char.autoSell;  end,
                         set = function( _, value )
                              AutoProfitLite.db.char.autoSell = value;
                              AutoProfitLite:OnShowButton();
                         end,
                    },
                    report = {
                         type = "toggle", order = 2,
                         name = L["APL_AUTOSELL_REPORT"],
                         desc = L["APL_AUTOSELL_REPORT"],
                         hidden = function() return not AutoProfitLite.db.char.autoSell; end, 
                         get = function () return AutoProfitLite.db.char.autoSellReport;  end,
                         set = function( _, value )
                              AutoProfitLite.db.char.autoSellReport = value;
                         end,
                    },
                    profit = {
                         type = "toggle", order = 3,
                         name = L["APL_PROFIT"],
                         desc = L["APL_PROFIT_DESC"],
                         get = function () return AutoProfitLite.db.char.showTotal; end,
                         set = function ( _, value )
                              AutoProfitLite.db.char.showTotal = value;
                         end,
                    },
                    sales = {
                         type = "group", order = 4, inline = true,
                         name = L["APL_PROFIT_LABEL"],
                         hidden = function() return not AutoProfitLite.db.char.showTotal; end,
                         args = {
                              profits = {
                                   type = "select", order = 2,
                                   name = L["APL_FANCY_PROFIT"],
                                   desc = L["APL_FANCY_PROFIT_DESC"],
                                   get = function() return AutoProfitLite.db.char.profits; end,
                                   set = function( _, value ) AutoProfitLite.db.char.profits = value; end,
                                   values = function()
                                        local preList = {};
                                        local v;
                                        for _, v in pairs( Config.printType ) do
                                             if ( v.value ~= AutoProfitLite.db.char.profits ) then
                                                  preList[v.value] = v.name
                                             else
                                                  preList[v.value] = ( "|cffffff9a%s|r" ):format( v.name );
                                             end
                                        end
                                        return preList;
                                   end,
                              },
                              colorprofits = {
                                   type = "select", order = 3,
                                   name = L["APL_COLOR_TEXT"],
                                   desc = L["APL_COLOR_TEXT_DESC"],
                                   get = function() return AutoProfitLite.db.char.cText; end,
                                   set = function( _, value ) AutoProfitLite.db.char.cText = value; end,
                                   values = function()
                                        local preList = {};
                                        local v;
                                        for _, v in pairs( Config.colorType ) do
                                             if ( v.value ~= AutoProfitLite.db.char.cText ) then
                                                  preList[v.value] = v.name
                                             else
                                                  preList[v.value] = ( "|cffffff9a%s|r" ):format( v.name );
                                             end
                                        end
                                        return preList;
                                   end,
                              },
                         },
                    },
                    button = {
                         type = "group", order = 5, inline = true,
                         name = L["APL_BUTTON_SETTINGS"],
                         args = {
                              buttonSpinRate = {
                                   type = "range", order = 1,
                                   name = L["APL_GOLDPILE"],
                                   desc = L["APL_GOLDPILE_DESC"],
                                   min = 0,
                                   max = 1,
                                   step = .1,
                                   get = function() return AutoProfitLite.db.char.spinRate; end,
                                   set = function( _, value )
                                        AutoProfitLite_SellTab_SellButton_TreasureModel.rotRate = value;
                                        AutoProfitLite.db.char.spinRate = value;
                                   end,
                              },
                              buttonSpin = {
                                   type = "select", order = 2,
                                   name = L["APL_BUTTONA"],
                                   desc = L["APL_BUTTONA_DESC"],
                                   get = function() return AutoProfitLite.db.char.buttonSpin; end,
                                   set = function( _, value ) AutoProfitLite.db.char.buttonSpin = value; end,
                                   values = function()
                                        local preList = {};
                                        local v;
                                        for _, v in pairs( Config.Spinner ) do
                                             if ( v.value ~= AutoProfitLite.db.char.buttonSpin ) then
                                                  preList[v.value] = v.name;
                                             else
                                                  preList[v.value] = ( "|cffffff9a%s|r" ):format( v.name );
                                             end
                                        end
                                        return preList;
                                   end,
                              },
                         },
                    },
               },
          },
          cat4 = {
               type = "group", order = 5,
               name = L["APL_ABOUT_MINIMAP_ICON"],
               args = {
                    confgendesc = {
                         type = "description", order = 1,
                         name = L["APL_ABOUT_LDB_DESC"],
                         cmdHidden = true,
                    },
                    hidemini = {
                         type = "toggle", order = 2,
                         name = L["APL_ABOUT_MINIMAP"],
                         desc = L["APL_ABOUT_MINIMAP_DESC"],
                         get = function() return AutoProfitLite.db.global.apxb.hide; end,
                         set = function( _, value )
                              if ( not value ) then
                                   LDBIcon:Show( AddOnName );
                              else
                                   LDBIcon:Hide( AddOnName );
                              end
                              AutoProfitLite.db.global.apxb.hide = value; 
                         end,
                    },
               },
          },
     },
}

-- AutoProfitLite:OnInitialize()
-- =========================================================
function AutoProfitLite:OnInitialize()
     -- Set up our database
     self.db = LibStub( "AceDB-3.0" ):New( DBName, aplDefaults, true );

     -- Set up our config options.
     local AutoProfitLiteConf = LibStub( "AceConfig-3.0" );
     local AutoProfitLiteReg  = LibStub( "AceConfigRegistry-3.0" );
     local AutoProfitLiteDiag = LibStub( "AceConfigDialog-3.0" );

     AutoProfitLiteConf:RegisterOptionsTable( AddOnName, apl );

     AutoProfitLiteReg:RegisterOptionsTable( ( "%s apl" ):format( AddOnName ), apl );

     self.ConfigFrames = { Main = AutoProfitLiteDiag:AddToBlizOptions( ( "%s apl" ):format( AddOnName ), AddOnName ), }

     self:RegisterEvent( "MERCHANT_SHOW" );
     self:RegisterEvent( "MERCHANT_CLOSED" );
     self:RegisterEvent( "BAG_UPDATE" );
     self:RegisterEvent( "PLAYER_ENTERING_WORLD" );

     self:RegisterChatCommand( "AutoProfitLight", "SlashProc" );
     self:RegisterChatCommand( "apl", "SlashProc" );

     -- LDB launcher
     -- ====================
     if ( LDB ) then
          AutoProfitLiteLauncher = LDB:NewDataObject( "AutoProfitLite", {
               type = "launcher",
               icon = "Interface\\Icons\\INV_Misc_Bag_EnchantedMageweave",
               OnClick = function( clickedframe, button )
                    PlaySoundFile( "igCharacterInfoTab", "Master" );
                    InterfaceOptionsFrame_OpenToCategory( AutoProfitLite.ConfigFrames.Main );
               end,
               OnTooltipShow = function( tt )
                    tt:AddLine( L["APL_MINIMAP_TEXT1"]:format( Version ) );
                    tt:AddLine( L["APL_MINIMAP_TEXT2"] );
                    local profit = AutoProfitLite:GetProfit( 1 );
                    tt:AddLine( L["APL_MINIMAP_TEXT3"]:format( AutoProfitLite:GetMoneyString( profit, AutoProfitLite.db.char.profits ) ) );
               end,
          } )
          if ( LDBIcon and not IsAddOnLoaded( "Broker2FuBar" ) and not IsAddOnLoaded( "FuBar" ) ) then
               LDBIcon:Register( "AutoProfitLite", AutoProfitLiteLauncher, AutoProfitLite.db.global.apxb );
          end
     end
end

-- AutoProfitLite:SlashProc()
-- =========================================================
function AutoProfitLite:SlashProc()
     PlaySoundFile( "igCharacterInfoTab", "Master" );
     InterfaceOptionsFrame_OpenToCategory( AutoProfitLite.ConfigFrames.Main );
end

-- AutoProfitLite:OnEnable()
-- =========================================================
function AutoProfitLite:OnEnable()
     self:RegisterEvent( "MERCHANT_SHOW" );
     self:RegisterEvent( "MERCHANT_CLOSED" );
     self:RegisterEvent( "BAG_UPDATE" );
     self:RegisterEvent( "PLAYER_ENTERING_WORLD" );
end

-- AutoProfitLite:OnDisable()
-- =========================================================
function AutoProfitLite:OnDisable()
     self:UnregisterEvent( "MERCHANT_SHOW" );
     self:UnregisterEvent( "MERCHANT_CLOSED" );
     self:UnregisterEvent( "BAG_UPDATE" );
     self:UnregisterEvent( "PLAYER_ENTERING_WORLD" );
end

-- AutoProfitLite:BAG_UPDATE()
-- =========================================================
function AutoProfitLite:BAG_UPDATE()
     AutoProfitLite:ButtonState();
end

-- AutoProfitLite:PLAYER_ENTERING_WORLD()
-- =========================================================
function AutoProfitLite:PLAYER_ENTERING_WORLD()
     if ( AutoProfitLite.db.global.showGreeting ) then
          collectgarbage( "collect" );
          UpdateAddOnMemoryUsage();
          local memFormated = ( "%s kb" ):format( floor( GetAddOnMemoryUsage( "AutoProfitLite" ) ) );
          AutoProfitLite:Print( "print", L["APL_BANNER_FORMAT"]:format( Version, memFormated ) );
     end
end

-- AutoProfitLite:MERCHANT_SHOW()
-- =========================================================
function AutoProfitLite:MERCHANT_SHOW()
     if ( AutoProfitLite.db.char.autoSell ) then
          local profit = AutoProfitLite:GetProfit( 0 );
          if ( profit > 0 ) then
               AutoProfitLite:SellJunk();
               if ( AutoProfitLite.db.char.showTotal ) then
                    AutoProfitLite:Print( "print", L["APL_FORMATED_PROFITS"]:format( AutoProfitLite:GetMoneyString( profit, AutoProfitLite.db.char.profits ) ) );
               end
          end
     else
          AutoProfitLite_SellTab:Show();
     end
end

-- AutoProfitLite:MERCHANT_CLOSED()
-- =========================================================
function AutoProfitLite:MERCHANT_CLOSED()
     if ( AutoProfitLite_SellTab:IsVisible() ) then
          AutoProfitLite_SellTab:Hide();
     end
end

-- AutoProfitLite:SellJunk()
-- =========================================================
function AutoProfitLite:SellJunk()
     local link, money
     if ( MerchantFrame:IsVisible() and MerchantFrame.selectedTab == 1 ) then
          for bag = 0, 4 do
               for slot = 1, GetContainerNumSlots( bag ) do
                    link = GetContainerItemLink( bag, slot );
                    if ( link and AutoProfitLite:IsJunk( bag, slot, link ) ) then
                         iValue = select( 11, GetItemInfo( link ) )
                         if ( iValue > 0 ) then
                              -- if slot not empty and item is junk then sell item
                              -- =============================
                              UseContainerItem( bag, slot );
                              if ( AutoProfitLite.db.char.autoSellReport ) then
                                        AutoProfitLite:Print( "print", L["APL_FORMATED_AUTOSELF_REPORT"]:format( link ) );
                              end
                         end
                    end
               end
          end
     end
end

-- AutoProfitLite:IsJunk()
-- =========================================================
function AutoProfitLite:IsJunk( bag, slot, link )
     if ( select( 3, GetItemInfo( link ) ) == 0 ) then
          return true;
     end
end

-- AutoProfitLite:GetProfit( tooltip )
-- =========================================================
function AutoProfitLite:GetProfit( tooltip )
     totalProfit = 0
     local isOpen, curTab
     if ( tooltip == 1 ) then
          isOpen = true;
          curTab = 1;
     else
          isOpen = MerchantFrame:IsVisible()
          curTab = MerchantFrame.selectedTab;
     end
     if ( isOpen and curTab == 1 ) then
          local bagSlots, link
          for bag = 0, 4 do
               bagSlots = GetContainerNumSlots( bag );
               if ( bagSlots > 0 ) then
                    for slot = 1, bagSlots do
                         link = GetContainerItemLink( bag, slot );
                         if ( link and AutoProfitLite:IsJunk( bag, slot, link ) ) then
                              AutoProfitLite_Tooltip:SetBagItem( bag, slot );
                         end
                    end
               end
          end
     end
     return totalProfit;
end

-- AutoProfitLite:OnEnterButton( self )
-- =========================================================
function AutoProfitLite:OnEnterButton( self )
     GameTooltip:SetOwner( self, "ANCHOR_RIGHT" );
     GameTooltip:SetText( L["APL_BUTTON_SELLJUNK"] );
     local profit = AutoProfitLite:GetProfit( 0 );
     if ( AutoProfitLite.db.char.buttonSpin == 2 ) then AutoProfitLite:UpdateButton(); end
     if ( profit > 0 ) then
          if ( AutoProfitLite.db.char.buttonSpin == 1 ) then AutoProfitLite:UpdateButton(); end
          GameTooltip:AddLine( AutoProfitLite:GetMoneyString( profit, AutoProfitLite.db.char.profits ) )
     else
          GameTooltip:AddLine( L["APL_BUTTON_NOJUNK"], 1.0, 1.0, 1.0, 1 );
     end
     GameTooltip:Show();
end

-- AutoProfitLite:OnLeaveButton()
-- =========================================================
function AutoProfitLite:OnLeaveButton()
     GameTooltip:Hide();
     if ( AutoProfitLite.db.char.buttonSpin ~= 3 ) then AutoProfitLite:UpdateButton( "stop" ); end
end

-- AutoProfitLite:OnClickButton( self, button )
-- =========================================================
function AutoProfitLite:OnClickButton( self, button )
     local profit = AutoProfitLite:GetProfit( 0 );
     if ( profit > 0 ) then
          GameTooltip:Hide();
          AutoProfitLite:SellJunk();
          if ( AutoProfitLite.db.char.showTotal ) then
               AutoProfitLite:Print( "print", L["APL_FORMATED_PROFITS"]:format( AutoProfitLite:GetMoneyString( profit, AutoProfitLite.db.char.profits ) ) );
          end
          AutoProfitLite:ButtonState();
     end
end

-- AutoProfitLite:OnShowButton()
-- =========================================================
function AutoProfitLite:OnShowButton()
     if ( AutoProfitLite.db.char.autoSell ) then
          if ( AutoProfitLite_SellTab:IsVisible() ) then
               AutoProfitLite_SellTab:Hide();
          end
     else
          AutoProfitLite_SellTab:Show();
     end
     if ( AutoProfitLite.db.char.buttonSpin == 3 and AutoProfitLite:GetProfit( 0 ) > 0 ) then
          AutoProfitLite:UpdateButton();
     end
end

-- AutoProfitLite:ButtonState()
-- =========================================================
function AutoProfitLite:ButtonState()
     -- Needed for OnEnterButton
     if ( AutoProfitLite.db.char.buttonSpin == 1 ) then
          AutoProfitLite:UpdateButton();
     end
     -- Needed for OnLeaveButton
     if ( AutoProfitLite.db.char.buttonSpin ~= 3 ) then
          AutoProfitLite:UpdateButton( "stop" );
     end
     -- Needed for OnClick
     if ( AutoProfitLite.db.char.buttonSpin ~= 2 ) then
          AutoProfitLite:UpdateButton( "stop" );
     end
     -- Needed for OnShowButton
     if ( AutoProfitLite.db.char.buttonSpin == 3 and AutoProfitLite:GetProfit( 0 ) > 0 ) then
          AutoProfitLite:UpdateButton();
     end
end

-- AutoProfitLite:AddTooltipMoney( S )
-- =========================================================
function AutoProfitLite:AddTooltipMoney( S )
     if ( S ) then totalProfit = totalProfit + S; end
end

-- AutoProfitLite:UpdateButton( action, spinRate )
-- =========================================================
function AutoProfitLite:UpdateButton( Action, spinRate )
     if ( Action == "stop" ) then
          AutoProfitLite_SellTab_Button_TreasureModel.rotRate = 0;
     else
          spinRate = tonumber( spinRate );
          if ( not spinRate ) then spinRate = AutoProfitLite.db.char.spinRate end
          AutoProfitLite_SellTab_Button_TreasureModel.rotRate = spinRate;
     end
end

-- AutoProfitLite:Print( Action, Msg )
-- =========================================================
function AutoProfitLite:Print( Action, Msg )
     if ( Action == "bug" and AutoProfitLite.db.char.debug.state ) then
          DEFAULT_CHAT_FRAME:AddMessage( L["APL_PRINT_DEBUGLINE"]:format( Msg) );
     elseif ( Action == "print" ) then
          DEFAULT_CHAT_FRAME:AddMessage( L["APL_PRINT_PRINTLINE"]:format( Msg ) );
     end
end

-- AutoProfitLite:GetMoneyString( Money, pType )
-- =========================================================
function AutoProfitLite:GetMoneyString( Money, pType )
     local gold = floor( Money / 10000 );
     local silver = floor( ( Money - gold * 10000 ) / 100 );
     local copper = mod( Money, 100 );
     local g, s, c, totalStr, mClr;
     mClr = AutoProfitLite.db.char.cText;
     if ( pType == 2 ) then
          if ( gold > 0 ) then
               g = L[( "APL_GOLD_%s_COIN" ):format( mClr )]:format( gold, 0, 0 );
               s = L[( "APL_SILVER_%s_COIN" ):format( mClr )]:format( silver, 0, 0 );
               c = L[( "APL_COPPER_%s_COIN" ):format( mClr )]:format( copper, 0, 0 );
               totalStr = ( "%s %s %s" ):format( g, s, c );
          elseif ( silver > 0 ) then
               s = L[( "APL_SILVER_%s_COIN" ):format( mClr )]:format( silver, 0, 0 );
               c = L[( "APL_COPPER_%s_COIN" ):format( mClr )]:format( copper, 0, 0 );
               totalStr = ( "%s %s" ):format( s, c );
          else
               c = L[( "APL_COPPER_%s_COIN" ):format( mClr )]:format( copper, 0, 0 );
               totalStr = ( "%s" ):format( c );
          end
     elseif ( pType == 1 ) then
          if ( gold > 0 ) then
               g = L[( "APL_TEXT_%s_G" ):format( mClr )]:format( gold );
               s = L[( "APL_TEXT_%s_S" ):format( mClr )]:format( silver );
               c = L[( "APL_TEXT_%s_C" ):format( mClr )]:format( copper );
               totalStr = ( "%s %s %s" ):format( g, s, c );
          elseif ( silver > 0 ) then
               s = L[( "APL_TEXT_%s_S" ):format( mClr )]:format( silver );
               c = L[( "APL_TEXT_%s_C" ):format( mClr )]:format( copper );
               totalStr = ( "%s %s" ):format( s, c );
          else
               c = L[( "APL_TEXT_%s_C" ):format( mClr )]:format( copper );
               totalStr = ( "%s" ):format( c );
          end
     end
     return totalStr;
end