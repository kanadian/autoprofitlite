﻿## Interface: 80000
## Title: AutoProfitLite
## Notes: Automatically sells low quality gray items to the vendor.
## Author: KanadiaN
## Version: r19
## SavedVariables: AutoProfitLiteDB
## OptionalDeps: Ace3
## X-Compatible-With: 80000
## X-Category: Inventory
## X-eMail: bug@kanadian.com
## X-Notes: Automatically sells low quality gray items to the vendor.
## X-WebSite: http://www.wowace.com/addons/autoprofitlite/
## X-Curse-Project-Name: AutoProfitLite
## X-Curse-Project-ID: autoprofitlite
## X-Curse-Repository-ID: wow/autoprofitlite/mainline

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

Locals\Locals.xml

AutoProfitLite.xml